import React from "react"
import { Link, useParams } from "react-router-dom"
const AboutItem = ({confirmedPhones,selectedPhone,setSelectedPhone})=>{
    const {phoneId} = useParams();
    const phone = confirmedPhones.find((phone) => phone.id.toString() === phoneId);
    if (!phone){
        return <div>Такого телефона не существует</div>
    }
    const handleAddToCart = () => {
        setSelectedPhone(prevSelectedPhones => [...prevSelectedPhones, phone]);
      };
    console.log(confirmedPhones.map((phone)=>console.log(phone.id)))
    return (
        <div className="container aboutitem">
            <div className="aboutitem__img"><img src={phone.image}></img></div>
            <div className="aboutitem__main">
                <h2>{phone.model}</h2>
                <div className="add">
                    <p>{phone.price+"₽"}</p>
                    <Link to={`/basket/` } onClick={() => handleAddToCart()} ><button type="button">Добавить в корзину</button></Link>
                </div>
                <h3>
                    {phone.description}
                </h3>
            </div>
        </div>
    )
}
export default AboutItem