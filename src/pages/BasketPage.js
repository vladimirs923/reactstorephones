import React, { useEffect } from "react"
import { AiFillDelete} from "react-icons/ai"
const BasketPage = ({selectedPhone,setSelectedPhone}) => {
  const deletePhone = (id) => {
    setSelectedPhone([...selectedPhone.filter((phone)=>phone.id!==id)])
    
  }
  //если у нас массив с телефонами, которые мы добавили в корзину пуст
  if (selectedPhone.length === 0) {
    return (
      <div className="container">
        <h1>Корзина товаров</h1>
        <p>Корзина пуста</p>
      </div>
    );
  }
  //Считаю сумму заказа
  const orderTotal = selectedPhone.reduce((total, phone) => total + parseFloat(phone.price),0);
  const countItems = selectedPhone.reduce((total,phone) => total + 1,0)
    return(
        <div className="container">
            <h1>Корзина товаров</h1>
            <div>
              {selectedPhone.map((phone) => (
              <div key={phone.id} className="basket">
                <img src={phone.image}></img>
                <h2>{phone.model}</h2>
                <p>{phone.price+ "₽"}</p>
                <AiFillDelete onClick={()=>deletePhone(phone.id)} className="btn"/>
              </div>
        ))}
      </div>
            <div>
                <p>Сумма заказа: {orderTotal+"₽"} </p>
                <p>Количество выбранных товаров: {countItems}</p>
            </div>
    </div>
    )
}
export default BasketPage