import SearchBtn from "../components/SearchBtn"
import React from "react"
import { useState } from "react"
import { useEffect } from "react"
import { Link, useNavigate } from "react-router-dom"
const CreateItem = ({confirmedPhones,setConfirmedPhones}) => {
    //Состояние для хранения запроса пользователя(для поиска)
    const [searchQuery,setSearchQuery] = useState('')
    //Состояние для фильтрования массива в зависимости от ввода
    const [filteredPhones, setFilteredPhones] = useState([]);
    //Создаю состояние,которое будет открывать выпадающий список
    const [isOpen,setIsOpen] = useState(false)
    const [currentPage, setCurrentPage] = useState(1);
    const [itemsPerPage, setItemsPerPage] = useState(4);  
    //Функция,которая будет переключать этот выпадающий список
    const toggleOption = () => {
        setIsOpen(!isOpen)
    }
    //состояние для хранения информации о выбранной опции сортировки:
    const [sortOption, setSortOption] = useState(null);
    const handleSortOption = (option) => {
        setSortOption(option);
        setIsOpen(false);
      };
    //фильтрация при выборе определенного пункта  
    useEffect(() => {
    const filtered = confirmedPhones.filter((phone) =>
      phone.model.toUpperCase().includes(searchQuery.toUpperCase())
    );
    if (sortOption === "ascending") {
        filtered.sort((a, b) => a.price - b.price);
      } else if (sortOption === "descending") {
        filtered.sort((a, b) => b.price - a.price);
      }    
    setFilteredPhones(filtered);
    setCurrentPage(1);
  }, [searchQuery, confirmedPhones,sortOption]);
  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = filteredPhones.slice(indexOfFirstItem, indexOfLastItem);
  const totalPages = Math.ceil(filteredPhones.length / itemsPerPage);
    return(
        <div className="container"> 
            <div className="item-filter">   
                <SearchBtn searchQuery={searchQuery} setSearchQuery={setSearchQuery}/>
                <div className={`price-filter ${isOpen ? 'active' : ''}`} onClick={toggleOption}>
                    <label htmlFor="price-range">Фильтровать по</label>
                    {isOpen && (
                         <ul className="price-options">
                            <li><a href="#" onClick={() => handleSortOption("ascending")}>Возрастанию цены</a></li>
                            <li><a href="#" onClick={() => handleSortOption("descending")}>Убыванию цены</a></li>
                         </ul>
                    )}
            </div>
            </div> 
            <div className="confirm-item">
              {filteredPhones.slice(indexOfFirstItem, indexOfLastItem).map((phone) => (
                <Link to={`/aboutitem/${phone.id}`} key={phone.id}>
                <div key={phone.id} className="phone-item">
                  <img src={phone.image} alt="" />
                  <p className="model">{phone.model}</p>
                  <p className="price">{phone.price + "₽"}</p>
                </div>
                </Link>
              ))}
            </div>
            {totalPages > 1 && (
              <div className="pagination">
                {Array.from({ length: totalPages }).map((_, index) => (
                  <button key={index} className={currentPage === index + 1 ? "active" : ""} onClick={() => handlePageChange(index + 1)}>{index + 1}</button>
                ))}
              </div>
             )}
        </div>
    )
}
export default CreateItem