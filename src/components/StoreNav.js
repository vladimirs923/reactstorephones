import React from "react"
import { Link } from "react-router-dom"
const StoreNav = ({confirmedPhones}) => {
    return(
        <div className="nav">
            <ul>
                <li><Link to="products"> <a href="#">Главная</a> </Link></li>
                <li><Link to="/"><a href="#">Создать</a></Link></li>
                <li><Link to={`/basket/`}><a href="#">Корзина</a></Link></li>
            </ul>
        </div>
    )}

export default StoreNav