import React from "react"
import { AiFillDelete,AiFillEdit,AiOutlineCheck} from "react-icons/ai"
const PhoneItem = ({phone,deletePhone,editPhone,confirmPhone,setConfirmedPhones}) => {
    return (
        <div className="phone-item">
            <img src={phone.image} alt="" />
            <p className="model">{phone.model}</p>
            <p className="price">{phone.price+"₽"}</p>
            <div className="btns">
                <AiFillDelete className="button_1" onClick={()=>deletePhone(phone.id)}/>
                <AiFillEdit  className="button_1" onClick={()=>editPhone(phone)}/>
                <AiOutlineCheck  className="button_1" onClick={()=>confirmPhone(phone)}/>
            </div>
        </div>
    )
}
export default PhoneItem